package Tasks_09_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Shop {
    /*Define items of a shop (they have a name and a price).
    Create a console application where user can see the list of items
    in the shop and choose which ones he/she wants to add to the shopping card.
    User should be able to list his/her shopping card and see the total price.*/

    private String name;
    private Map<String, Double> goods = new HashMap<>();

    Shop(String name) {
        this.name = name;
    }

    public void info() {
        System.out.println(this.name);
        System.out.println(this.goods);
        System.out.println();
    }

    public static Shop[] generate() {
        Shop wallmart = new Shop("wallmart");
        Shop lidl = new Shop("lidl");
        Shop maxima = new Shop("maxima");
        Shop[] arr = new Shop[] {wallmart, lidl, maxima};

        for (int a = 0; a < 3; a++){
            arr[a].goods.put("milk", Math.round((1 + Math.random() * 2) * 100.0) / 100.0);
            arr[a].goods.put("meat",  Math.round((3 + Math.random() * 10) * 100.0) / 100.0);
            arr[a].goods.put("eggs",  Math.round((1 + Math.random() * 3) * 100.0) / 100.0);
            arr[a].goods.put("bread",  Math.round(( Math.random() * 2) * 100.0) / 100.0);
            arr[a].goods.put("sweets",  Math.round((2 + Math.random() * 4) * 100.0) / 100.0);
        }
        return arr;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Shop[] shops = generate();
        for (Shop x : shops) {
            x.info();
        }

        System.out.println("Choose u shop: ");
        String shopName = reader.readLine();
        System.out.println("Type goods and count: ");
        System.out.println("If u want rewrite amount of goods just type of thats goods count again");
        System.out.println("For exit type break");
        Map<String, Double> cart = new HashMap<>();

        for (;;) {
            String input = reader.readLine();
            if (input.equals("break")) break;
            int index = 0;
            for (int a = 0; a < shops.length; a++) {
                if (shops[a].name.equals(shopName)) {
                    index = a;
                }
            }

            cart.put(input, shops[index].goods.get(input) * Integer.parseInt(reader.readLine()));
            System.out.println("Type goods and count: ");
            System.out.println("For exit type break");
        }

        Double sum = 0.0;
        for (Map.Entry<String, Double> entry : cart.entrySet()) {
            sum += entry.getValue();
        }

        System.out.println();
        System.out.println("Your cart:");
        System.out.println(cart);
        System.out.println("Total sum of goods: " + sum);
    }
}
