package Tasks_09_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task2 {
    /*Write a function that takes array of ints as an input (argument),
    The function should return true if it consists of only consecutive
    element groups of 2 exact same elements. Example: 11224433 is true,
            11558 is false (only one 8 ). 44555 is false(three fives), 7777
    is false (4 sevens)*/

    public static boolean preCheck(int[] arr) {
        int count = 1;

        for (int a = 0; a < arr.length-1; a++) {
            if (count == 0 || count > 2) return false;
            if (arr[a] == arr[a + 1]) {
                count++;
            }
            else count--;
        }
        return true;
    }

    public static boolean checkArr(int[] arr) {
        int count = 0;

        if (arr.length % 2 == 0 && preCheck(arr)) {
            for (int a = 0; a < arr.length -1; a += 2) {
                if (arr[a] == arr[a + 1]) {

                }
                else return false;
            }
        }
        else return false;

        return true;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String asArr = reader.readLine();

        int[] arr = new int[asArr.length()];

        for (int a = 0; a < arr.length; a++) {
            arr[a] = asArr.charAt(a) - '0';
        }

        System.out.println(checkArr(arr));
    }
}
