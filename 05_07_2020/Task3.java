package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task3 {
    /*Write a simple calculator program, where user can enter
        <number> <operator> <number> end your program calculates the result
    and prints it. Numbers can be decimal, and operators supported should be +, -, *, /, %.*/

    public static void main (String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Plz type <num> <operator> <num>");
        int num1 = Integer.parseInt(reader.readLine());
        String operator = reader.readLine();
        int num2 = Integer.parseInt(reader.readLine());

        if (operator.contains("+")) {
            System.out.println(num1+num2);
        }
        if (operator.contains("-")) {
            System.out.println(num1-num2);
        }
        if (operator.contains("*")) {
            System.out.println(num1*num2);
        }
        if (operator.contains("/")) {
            System.out.println(num1/num2);
        }
        if (operator.contains("%")) {
            System.out.println(num1%num2);
        }
    }
}
