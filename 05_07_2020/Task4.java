package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Task4 {
    /*Write a program that generates a random number and asks the user
    to guess what the number is. If the user’s guess is higher than
    the random number, the program should display “Too high, try again.
    ” If the user’s guess is lower than the random number, the program
    should display “Too low, try again.” The program should use a loop that
    repeats until the user correctly guesses the random number.*/

    public static void main (String[] args) throws IOException {

        Random rnd = new Random();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Try a number from 0-10:");
        int num = Integer.parseInt(reader.readLine());
        int b = rnd.nextInt(10);

        while (num != b) {
            if (num  > b) {
                System.out.println("Too hugh");
            }
            else
                System.out.println("Too low");

            num = Integer.parseInt(reader.readLine());
        }

        System.out.println("U good, number was: " + b);
    }
}
