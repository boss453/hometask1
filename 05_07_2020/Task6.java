package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Task6 {
   /* Write a Java program to find the largest and smallest
    element of an array. (Array can be defined by you or by the user)*/

   public static void main (String[] args) throws IOException {

       BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
       System.out.println("Write arr size: ");
       int size = Integer.parseInt(reader.readLine());
       ArrayList<Integer> arr = new ArrayList<>();

       for (int a = 0; a < size; a++) {
           Random rnd = new Random();
           int b = rnd.nextInt(10);
           arr.add(b);
       }
       System.out.println(Collections.max(arr));
   }
}
