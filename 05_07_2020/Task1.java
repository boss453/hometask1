package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task1 {

    public static void main (String[] args) throws IOException {

        //        Write a program to find the factorial
        // value of any number entered through the keyboard.

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(reader.readLine());
        int fact = 1;

        for (int a = 1; a <= num; a++) {
            fact *= a;
        }

        System.out.println(fact);
    }
}
