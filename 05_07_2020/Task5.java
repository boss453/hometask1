package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Task5 {
   /* Write a program that creates an array of 10 elements size.
    Your program should prompt the user to input numbers in array
    and then create a new array that will have the same elements
    but in a reverse order. Print out the second array.*/

   public static void main (String[] args) throws IOException {

       BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
       ArrayList<Integer> arr = new ArrayList<>();

       System.out.println("input size for u array: ");
       int sizeArr = Integer.parseInt(reader.readLine());

       System.out.println("Now input numbers for u array: ");

       for (int a = 0; a < sizeArr; a++) {
           arr.add(Integer.parseInt(reader.readLine()));
       }

       Collections.reverse(arr);
       System.out.println(arr);
   }
}
