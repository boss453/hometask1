package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task7 {
   /* Write a Java program that plays tic-tac-toe with you,
    Both players are controlled by a user, after each move
    you should display the state of the board.
            Example:
            > Player1(x) please make a move:
            > 1 2
            > . x .
            > . . .
            > . . .
        > Player2(o) please make a move:
            > 1 1
            > . x .
            > . o .
            > . . .
    example of coordinates:
            0 2 | 1 2 | 2 2
            0 1 | 1 1 | 2 2
            0 0 | 1 0 | 2 2*/

   public static void main (String[] args) throws IOException {

       BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

       char[][] arr = {{'q', 'q', 'q'}, {'q', 'q', 'q'}, {'q', 'q', 'q'}};
       int a, b, c, d;

       for (;;) {

           System.out.println();
           System.out.println("for exit type 33");
           System.out.println("Write coordinates from 0-2, 1 player: ");
            a = Integer.parseInt(reader.readLine());
            if (a == 33) {
                break;
            }
            b = Integer.parseInt(reader.readLine());
            arr[a][b] = 'x';
           System.out.println("for exit type 33");
           System.out.println("Write coordinates from 0-2, 2 player: ");
            c = Integer.parseInt(reader.readLine());
           if (c == 33) {
               break;
           }
            d = Integer.parseInt(reader.readLine());
            arr[c][d] = 'y';

           for (int i = 0; i < arr.length; i++) {
               System.out.println();
               for (int j = 0; j < arr.length; j++) {
                   System.out.print("[" + arr[i][j] + "]" + " ");
               }
           }
           System.out.println();
       }
   }
}
