package Tasks_05_07_2020;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task2 {

    public static void main (String[] args) throws IOException {
      /*  Write a program that prompts the user to input an integer and then outputs
        the number with the digits reversed.
        For example, if the input is 12345, the output should be 54321.*/

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Write number: ");
        String num = reader.readLine();

        for (int a = num.length() - 1; a >= 0 ; a--) {
            System.out.print(num.charAt(a) + " ");
        }
    }
}
